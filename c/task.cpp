#include <fstream>
#include <iostream>
#include <cassert>
using namespace std;

// Олимпиада кафедры ПОВТАС БГТУ им. В. Г. Шухова
// по программированию (18 сентября 2013 г.)

// Задача C (Чёрным по белому)

//   .oooooo.
//  d8P'  `Y8b
// 888
// 888
// 888
// `88b    ooo
//  `Y8bood8P'

// Интервал прямой с целочисленными координатами [a, b)
// содержит левую границу - точку a и не содержит правую границу - точку b.
// Интервал от 0 до 1000000000 выкрасили в белый цвет.
// Затем было выполнено N операций перекрашивания.
// При каждой операции цвета в интервале, границы которого задаются,
// меняются на противоположный (белый на чёрный, чёрный на белый).
// Требуется написать программу, которая найдёт самый длинный интервал
// белого цвета после заданной последовательности операций перекрашивания.

// Входной текстовый файл input.txt содержит
// в первой строке число N (1 <= N <= 500)
// и затем N строк с границами интервалов
// (числа в диапазоне от 0 до 1000000000).

#define INPUT_FILE_NAME "input.txt"

// Выходной текстовый файл output.txt должен содержать
// одно число - длину самого большого белого интервала.
#define OUTPUT_FILE_NAME "output.txt"

#define MIN_BOUND 0
#define MAX_BOUND 1000000000
#define UNKNOWN -1

struct Bound {
	long int value;
	bool isStart;
};

typedef Bound* BoundsArray;

void printBound(Bound bound) {
	if (bound.isStart) {
		std::cout << '(' << bound.value << ' ';
	} else {
		std::cout << bound.value << ')' ;
	}
}

void printBounds(BoundsArray bounds, int n) {
	for (int i = 0; i < n; i++) {
		printBound(bounds[i]);
	}
	std::cout << '\n';
}

// Упорядоченная вставка границы в упорядоченный массив границ
// Фактически используется сортировка вставками.
// Находится подходящее место для границы, либо граница добавляется самой последней
void insertBound(Bound bound, BoundsArray bounds, int n) {
	int i = 0;
	bool isPlaced = false;
	while (!isPlaced && (i < n)) {
		if (bound.value <= bounds[i].value || bounds[i].value == UNKNOWN) {
			for (int j = n - 2; j >= i; --j) {
				bounds[j + 1] = bounds[j];
			}
			bounds[i] = bound;
			isPlaced = true;
		}
		i++;
	}
}
// Поиск максимального интервала
// Определяется расстояние от минимального значения интервала до первого
// элемента массива границ. Затем, переходя через каждую границу, мы инвертируем
// цвет текущего интервала и если этот цвет белый, то находим расстояние до
// предыдущего значения
int getMaxInterval(BoundsArray bounds, int boundsCount) {
	unsigned long int max = 0, current = 0;
	bool isWhite = true;
	int i = 0;
	for (int i = 0; i < boundsCount - 1; i++) {
		if ((i % 2) == 0) {
			current += bounds[i + 1].value - bounds[i].value;
		}
		if ((i % 2 == 0) && (current > max)) {
			max = current;
		} else {
			if ((i % 2 != 0) && (bounds[i + 1].value - bounds[i].value != 0)) {
				current = 0;
			}
		}
	}
	return current > max ? current : max;
}

int main(int argc, char const *argv[]) {
	std::ifstream in(INPUT_FILE_NAME);
	std::ofstream out(OUTPUT_FILE_NAME);

	int n = 0;
	in >> n;

	int boundsCount = 2*n + 2;
	BoundsArray bounds = new Bound [boundsCount];

	Bound defaultBound = {
		UNKNOWN,
		false
	};
	for (int i = 1; i < boundsCount - 1; ++i) {
		bounds[i] = defaultBound;
	}
	Bound bound = {
		MIN_BOUND,
		true
	};
	bounds[0] = bound;
	bound.value = MAX_BOUND;
	bound.isStart = false;
	bounds[1] = bound;
	printBounds(bounds, boundsCount);
	bool isStart = true;
	for (int i = 1; i < boundsCount - 1; ++i) {
		Bound bound;
		in >> bound.value;
		bound.isStart = isStart;
		insertBound(bound, bounds, boundsCount);
		isStart = !isStart;
	}
	printBounds(bounds, boundsCount);

	unsigned long int maxInterval = getMaxInterval(bounds, boundsCount);

	out << maxInterval;

	return 0;
}